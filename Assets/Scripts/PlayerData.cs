[System.Serializable]
public class PlayerData
{
    public int hearts;
    public float maxHealth;
    public float health;
    public float maxMagity;
    public float magity;
    public float damage;
    public int money;

    public PlayerData(PlayerSystem player)
    {
        hearts = player.hearts;
        maxHealth = player.maxHP;
        health = player.hp;
        maxMagity = player.maxMagity;
        magity = player.magity;
        damage = player.damage;
        money = player.money;
    }

}
