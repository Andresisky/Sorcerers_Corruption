using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldSack : MonoBehaviour
{
    PlayerSystem player;
    Collider col;
    Rigidbody rb;
    int loot;
    GameObject pickUpSound;
    public int moneyLoot;
    public GameObject mesh;
    public bool onGround = false;

    [SerializeField] private AudioSource audioManager;
    [SerializeField] private AudioClip pickedSFX;

    [SerializeField] private int minLoot = 100;
    [SerializeField] private int maxLoot = 200;

    //[SerializeField] private Items item;
    public bool greedsAmulet;

    private void Awake()
    {
        //item = FindObjectOfType<Items>();
        moneyLoot = Random.Range(minLoot, maxLoot);
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerSystem>();
        col = GetComponent<Collider>();
        rb = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        col.isTrigger = false;
        rb.AddForce(Vector3.up * 4, ForceMode.Impulse);
    }

    private void Update()
    {
        //greedsAmulet = item.greedsAmulet;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (!greedsAmulet)
            {
                player.money += moneyLoot;
            }
            else
            {
                player.money += moneyLoot * 4;
            }
            audioManager.PlayOneShot(pickedSFX);
            mesh.SetActive(false);
            col.enabled = false;
            Destroy(gameObject, 2f);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.gameObject.layer == LayerMask.NameToLayer("WhatIsGround"))
        {
            Throw();
        }

        if (collision.collider.gameObject.layer == LayerMask.NameToLayer("WhatIsEnemy"))
        {
            Physics.IgnoreCollision(collision.collider, col);
        }

        if (collision.collider.gameObject.layer == LayerMask.NameToLayer("WhatIsPlayer"))
        {
            if (!onGround)
            {
                Physics.IgnoreCollision(collision.collider, col);
            }
        }
    }

    void Throw()
    {
        onGround = true;
        Physics.IgnoreCollision(player.gameObject.GetComponent<Collider>(), col, false);
        rb.isKinematic = true;
        col.isTrigger = true;
    }

}
