using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class PauseMenu : MonoBehaviour
{
    private static PauseMenu _instance;

    [SerializeField] private GameObject pauseCanvas;
    [SerializeField] private Volume pauseVolume;
    [SerializeField] private PlayerSystem playerSystem;

    public bool GameIsPaused = false;

    public static PauseMenu Instance
    {
        get
        {
            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        playerSystem = FindObjectOfType<PlayerSystem>();
    }

    private void Update()
    {
        if (PlayerInputs.Instance.PauseUnpause())
        {
            if (GameIsPaused)
            {
                ResumeGame();
            }
            else
            {
                PauseGame();
            }
        }
    }

    public void PauseGame()
    {
        playerSystem.DisableMovement();
        playerSystem.playerHUD.SetActive(false);

        pauseVolume.enabled = true;
        pauseCanvas.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    public void ResumeGame()
    {
        playerSystem.EnableMovement();
        playerSystem.playerHUD.SetActive(true);

        pauseVolume.enabled = false;
        pauseCanvas.SetActive(false);
        Time.timeScale = 1.0f;
        GameIsPaused = false;

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }
}
