using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SkillCooldown : MonoBehaviour
{
    public static SkillCooldown instance;
    public static SkillCooldown Instance
    {
        get
        {

            if (instance == null)
            {
                instance = FindObjectOfType<SkillCooldown>();
                if (instance != null)
                {
                    instance = new GameObject().AddComponent<SkillCooldown>();
                }
            }

            return instance;
        }
    }


    public void CooldownSkill(Image cooldownImage, int cooldown)
    {
        cooldownImage.fillAmount -= 1 / cooldown * Time.deltaTime;
        if (cooldownImage.fillAmount <= 0)
        {
            cooldownImage.fillAmount = 0;
        }
    }
}
