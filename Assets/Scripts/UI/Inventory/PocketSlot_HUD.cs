using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PocketSlot_HUD : MonoBehaviour
{
    [SerializeField] private Image itemSprite;
    [SerializeField] private TextMeshProUGUI itemCount;
    [SerializeField] private GameObject mask;

    [SerializeField] private InventorySlot_UI pocket;

    [SerializeField] private InventorySlot assignedInventorySlot;



    public Image ItemSprite
    {
        get { return itemSprite; }
        set { itemSprite = value; }
    }

    public TextMeshProUGUI ItemCount
    {
        get { return itemCount; }
        set { itemCount = value; }
    }

    private void Awake()
    {
        ClearSlot();
        mask.SetActive(false);
    }

    private void Update()
    {
        assignedInventorySlot = pocket.AssignedInventorySlot;
        if (pocket.AssignedInventorySlot != null)
        {
            mask.SetActive(true);
            ItemSprite.color = pocket.ItemSprite.color;
            ItemSprite.sprite = pocket.ItemSprite.sprite;
            ItemCount.text = pocket.ItemCount.text;
        }
        if (assignedInventorySlot.ItemData == null)
        {
            mask.SetActive(false);
            ClearSlot();
        }
    }

    public void Init(InventorySlot slot)
    {
        assignedInventorySlot = slot;
        UpdateUISlot(slot);
    }

    public void UpdateUISlot(InventorySlot slot)
    {
        if (slot.ItemData != null)
        {
            itemSprite.sprite = slot.ItemData.itemIconPNG;
            itemSprite.color = Color.white;
            if (slot.StackSize > 1)
            {
                itemCount.text = slot.StackSize.ToString();
            }
            else
            {
                itemCount.text = "";
            }
        }
        else
        {
            ClearSlot();
        }
    }

    public void UpdateUISlot()
    {
        if (assignedInventorySlot != null)
        {
            UpdateUISlot(assignedInventorySlot);
        }
    }

    public void ClearSlot()
    {
        assignedInventorySlot?.ClearSlot();
        itemSprite.sprite = null;
        itemSprite.color = Color.clear;
        itemCount.text = "";
    }

}
