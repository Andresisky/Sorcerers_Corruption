using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class InventorySlot
{
    [SerializeField] private ItemsSO itemData;      // Reference to the data
    [SerializeField] private int stackSize;     // Current stack Size - How many of the data we have?

    public ItemsSO ItemData => itemData;
    public int StackSize => stackSize;

    public InventorySlot(ItemsSO source, int amount)    // Constructor to make a ocuppied inventory slot
    {
        itemData = source;
        stackSize = amount;
    }

    public InventorySlot()  // Constructor to make an empty inventory slot
    {
        ClearSlot();
    }

    public void ClearSlot() // Clears the Slot
    {
        itemData = null;
        stackSize = -1;
    }

    public void AssignItem(InventorySlot invSlot)   // Assigns an item to the slot
    {
        if (itemData == invSlot.ItemData) AddToStack(invSlot.StackSize);    // Does the slot contain the same item? Add to stack
        else    // Overrides slot with the inventory slot we are passing in
        {
            itemData = invSlot.ItemData;
            stackSize = 0;
            AddToStack(invSlot.StackSize);
        }
    }

    public void UpdateInventorySlot(ItemsSO data, int amout)    // Update slot directly
    {
        itemData = data;
        stackSize = amout;
    }

    public bool EnoughRoomLeftInStack(int amountToAdd, out int amountRemaining)   // Would there be enough room in the stack for the amount we are trying to add.
    {
        amountRemaining = itemData.MaxStackSize - stackSize;
        return EnoughRoomLeftInStack(amountToAdd);
    }

    public bool EnoughRoomLeftInStack(int amountToAdd)
    {

        if (ItemData == null || stackSize + amountToAdd <= itemData.MaxStackSize)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void AddToStack(int amount)
    {
        stackSize += amount;
    }

    public void RemoveFromStack(int amount)
    {
        stackSize -= amount;
    }

    public bool SplitStack(out InventorySlot splitStack)
    {
        if (StackSize <= 1) // Is there enough to actually split? If not return false.
        {
            splitStack = null;
            return false;
        }
        
        int halfStack = Mathf.RoundToInt(StackSize / 2); // Get half the stack.
        RemoveFromStack(halfStack);

        splitStack = new InventorySlot(itemData, halfStack);    // Creates a copy of this slot with half the tack size.
        return true;
    }
}
