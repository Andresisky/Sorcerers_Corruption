using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

[System.Serializable]
public class InventoryHolder : MonoBehaviour
{
    [SerializeField] private int inventorySize;
    [SerializeField] protected InventoryManager inventoryManager;

    [SerializeField] private int pocketsSize;
    [SerializeField] protected InventoryManager pocketManager;


    public InventoryManager InventoryManager => inventoryManager;
    public InventoryManager PocketManager => pocketManager;
    public static UnityAction<InventoryManager> OnDynamicInventoryDisplayRequested;

    //Constructors
    public int InventorySize
    {
        get { return inventorySize; }
        set { inventorySize = value; }
    }

    public int PocketSize
    {
        get { return pocketsSize; }
        set { pocketsSize = value; }
    }




    private void Awake()
    {
        inventoryManager = new InventoryManager(inventorySize);
        pocketManager = new InventoryManager(pocketsSize);
    }

    private void Update()
    {

        if (PlayerInputs.Instance.Item1())
        {
            if (pocketManager.InventorySlots[0].ItemData != null)
            {
                CheckItemInPocket(pocketManager.InventorySlots[0].ItemData);
            }
                
        }
        if (PlayerInputs.Instance.Item2())
        {
            if (pocketManager.InventorySlots[1].ItemData != null)
            {
                CheckItemInPocket(pocketManager.InventorySlots[1].ItemData);
            }  
        }

        
    }

    public void CheckItemInPocket(ItemsSO itemData)
    {
        string pocket = itemData.id;
        if (pocket == "item_health")
        {
            // HEALTH POTION
            ItemDatabase.Instance.HealthPotion();
            ItemDatabase.Instance.DrinkSFX(itemData);

            pocketManager.RemoveFromInventory(itemData, 1);
        }

        if (pocket == "item_magic")
        {
            // MAGITY POTION
            ItemDatabase.Instance.MagityPotion(itemData);
            ItemDatabase.Instance.DrinkSFX(itemData);

            pocketManager.RemoveFromInventory(itemData, 1);
        }

        if (pocket == "item_fury")
        {
            // BOTTLED FURY POTION
            ItemDatabase.Instance.DrinkSFX(itemData);

            pocketManager.RemoveFromInventory(itemData, 1);
        }

        if (pocket == "item_blessing")
        {
            // VALLEY'S BLESSING POTION
            ItemDatabase.Instance.ValleyPotion();
            ItemDatabase.Instance.DrinkSFX(itemData);

            pocketManager.RemoveFromInventory(itemData, 1);
        }

        if (pocket == "item_amulet")
        {
            // GREED'S AMULET
        }
    }
}
