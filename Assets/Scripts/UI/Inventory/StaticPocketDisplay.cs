using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticPocketDisplay : InventoryDisplay
{
    [SerializeField] private InventoryHolder pocketHolder;
    [SerializeField] private InventorySlot_UI[] pockets;

    protected override void Start()
    {
        base.Start();

        if (pocketHolder != null)
        {
            inventoryManager = pocketHolder.PocketManager;
            inventoryManager.OnInventorySlotChanged += UpdateSlot;
        }
        else
        {
            Debug.LogWarning($"No pocket assigned to {this.gameObject}");
        }

        AssignSlot(inventoryManager);
    }

    public override void AssignSlot(InventoryManager invToDisplay)
    {
        slotDictionary = new Dictionary<InventorySlot_UI, InventorySlot>();

        if (pockets.Length != inventoryManager.InventorySize) Debug.Log($"Pocket Slots out of sync on {this.gameObject}");
        for (int i = 0; i < inventoryManager.InventorySize; i++)
        {
            SlotDictionary.Add(pockets[i], inventoryManager.InventorySlots[i]);
            pockets[i].Init(inventoryManager.InventorySlots[i]);
        }
    }

    private void Update()
    {
        for (int i = 0; i < pockets.Length; i++)
        {
            pockets[i].UpdateUISlot();
        }
    }
}
