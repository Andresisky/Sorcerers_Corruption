using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InventorySlot_UI : MonoBehaviour
{
    [SerializeField] private Image itemSprite;
    [SerializeField] private TextMeshProUGUI itemCount;
    [SerializeField] private TextMeshProUGUI itemName;

    [SerializeField] private InventorySlot assignedInventorySlot;
    private Button button;


    public Image ItemSprite
    {
        get { return itemSprite; }
        set { itemSprite = value; }
    }

    public TextMeshProUGUI ItemCount
    {
        get { return itemCount; }
        set { itemCount = value; }
    }

    public InventorySlot AssignedInventorySlot
    {
        get { return assignedInventorySlot; }
        set { assignedInventorySlot = value; }
    }
    public InventoryDisplay ParentDisplay { get; private set; }

    private void Awake()
    {
        ClearSlot();
        button = GetComponent<Button>();
        button?.onClick.AddListener(OnUISlotClick);

        ParentDisplay = transform.parent.GetComponent<InventoryDisplay>();
    }

    public void Init(InventorySlot slot)
    {
        assignedInventorySlot = slot;
        UpdateUISlot(slot);
    }

    public void UpdateUISlot(InventorySlot slot)
    {
        if (slot.ItemData != null)
        {
            itemSprite.sprite = slot.ItemData.itemIconPNG;
            itemSprite.color = Color.white;
            itemName.text = slot.ItemData.itemName;
            if (slot.StackSize > 1)
            {
                itemCount.text = slot.StackSize.ToString();
            }
            else
            {
                itemCount.text = "";
            }
        }
        else
        {
            ClearSlot();
        }
    }

    public void UpdateUISlot()
    {
        if (assignedInventorySlot != null)
        {
            UpdateUISlot(assignedInventorySlot);
        }
    }

    public void ClearSlot()
    {
        assignedInventorySlot?.ClearSlot();
        itemSprite.sprite = null;
        itemSprite.color = Color.clear;
        itemCount.text = "";
        itemName.text = "";
    }

    public void OnUISlotClick()
    {
        // Access display class function

        ParentDisplay?.SlotClicked(this);
    }

}
