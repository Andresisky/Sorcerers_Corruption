using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticInventoryDisplay : InventoryDisplay
{
    [SerializeField] private InventoryHolder inventoryHolder;
    [SerializeField] private InventorySlot_UI[] slots;

    protected override void Start()
    {
        base.Start();

        if (inventoryHolder != null)
        {
            inventoryManager = inventoryHolder.InventoryManager;
            inventoryManager.OnInventorySlotChanged += UpdateSlot;
        }
        else
        {
            Debug.LogWarning($"No inventory assigned to {this.gameObject}");
        }

        AssignSlot(inventoryManager);
    }

    public override void AssignSlot(InventoryManager invToDisplay)
    {
        slotDictionary = new Dictionary<InventorySlot_UI, InventorySlot>();

        if (slots.Length != inventoryManager.InventorySize) Debug.Log($"Inventory Slots out of sync on {this.gameObject}");
        for (int i = 0; i < inventoryManager.InventorySize; i++)
        {
            SlotDictionary.Add(slots[i], inventoryManager.InventorySlots[i]);
            slots[i].Init(inventoryManager.InventorySlots[i]);
        }
    }

    private void Update()
    {
        for (int i = 0; i < slots.Length; i++)
        {
            slots[i].UpdateUISlot();
        }
    }
}