using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class SliderBar : MonoBehaviour
{
    private float targetValue;
    private float timeScale = 0;
    private bool isLerping;


    public static SliderBar instance;
    public static SliderBar Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<SliderBar>();
                if (instance != null)
                {
                    instance = new GameObject().AddComponent<SliderBar>();
                }
            }

            return instance;
        }
    }

    public void SetMaxValue(Slider slider, float maxValue)
    {
        slider.maxValue = maxValue;
        slider.value = maxValue;
    }

    public void SetValue(Slider slider, float attribute)
    {
        targetValue = attribute;
        timeScale = 0;
        if  (!isLerping)
        {
            StartCoroutine(LerpSlider(slider));
        }
    }

    public IEnumerator LerpSlider(Slider slider)
    {
        float speed = 2;
        float startValue = slider.value;

        isLerping = true;

        while (timeScale < 1)
        {
            timeScale += Time.deltaTime * speed;
            slider.value = Mathf.Lerp(startValue, targetValue, timeScale);
        }
        isLerping = false;
        yield return new WaitForSeconds(timeScale);
    }
}
