using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.Audio;
using UnityEngine.Rendering.Universal;
using TMPro;

public class SettingsMenu : MonoBehaviour
{
    //Video Settings
    [SerializeField] GameObject videoPanel;
    [Space(10)]
    [SerializeField] private TMP_Dropdown resolutionDropdown;
    [SerializeField] private TMP_Dropdown graphicsDropdown;
    const string prefName = "GraphicsValue";
    Resolution[] resolutions;

    //Audio Settings
    [SerializeField] GameObject audioPanel;


    //Controls Settings
    [SerializeField] GameObject controlsPanel;

    //Other Settings
    [SerializeField] GameObject otherPanel;
    [SerializeField] private Button applyChangesButton;

    private void Awake()
    {
        graphicsDropdown.onValueChanged.AddListener(new UnityAction<int>(index =>
        {
            PlayerPrefs.SetInt(prefName, graphicsDropdown.value);
            PlayerPrefs.Save();
        }));

        //applyChangesButton.onClick.AddListener(delegate { SaveConfig(qualityIndex, isFullscreen, resolutionIndex); });
    }

    private void Start()
    {
        graphicsDropdown.value = PlayerPrefs.GetInt(prefName);

        resolutions = Screen.resolutions;
        resolutionDropdown.ClearOptions();
        List<string> options = new List<string>();
        int currentResolutionIndex = 0;
        for (int i = 0; i < resolutions.Length; i++)
        {
            string option = resolutions[i].width + " x " + resolutions[i].height;
            options.Add(option);

            if (resolutions[i].width == Screen.currentResolution.width && resolutions[i].height == Screen.currentResolution.height)
            {
                currentResolutionIndex = i;
            }
        }
        resolutionDropdown.AddOptions(options);
        resolutionDropdown.value = currentResolutionIndex;
        resolutionDropdown.RefreshShownValue();
    }


    public void SetQuality(int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
    }

    public void SetFullScreen(bool isFullscreen)
    {
        Screen.fullScreen = isFullscreen;
    }

    public void SetResolution(int resolutionIndex)
    {
        Resolution resolution = resolutions[resolutionIndex];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
    }

    public void SettingsButton(string panelToDisplay)
    {
        if (panelToDisplay == videoPanel.name)
        {
            videoPanel.SetActive(true);

            audioPanel.SetActive(false);
            controlsPanel.SetActive(false);
            otherPanel.SetActive(false);
        }

        if (panelToDisplay == audioPanel.name)
        {
            audioPanel.SetActive(true);

            videoPanel.SetActive(false);
            controlsPanel.SetActive(false);
            otherPanel.SetActive(false);
        }

        if (panelToDisplay == controlsPanel.name)
        {
            controlsPanel.SetActive(true);

            audioPanel.SetActive(false);
            videoPanel.SetActive(false);
            otherPanel.SetActive(false);
        }

        if (panelToDisplay == otherPanel.name)
        {
            otherPanel.SetActive(true);

            audioPanel.SetActive(false);
            videoPanel.SetActive(false);
            controlsPanel.SetActive(false);
        }
    }

    public void SaveConfig(int qualityIndex, bool isFullscreen, int resolutionIndex)
    {
        /*SetQuality(qualityIndex);
        SetFullScreen(isFullscreen);
        SetResolution(resolutionIndex);

        Debug.Log(qualityIndex);
        Debug.Log(isFullscreen);
        Debug.Log(resolutionIndex);*/
    }
}
