using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using TMPro;
using System;

public class VolumeSettings : MonoBehaviour
{
    [SerializeField] string _volumeParameter = "Set Parameter Here";
    [SerializeField] AudioMixer _mixer;
    [SerializeField] Toggle _toggle;
    private TMP_Text _toggle_Text;
    [SerializeField] Slider _slider;
    private TMP_Text volume_Percentage;
    [SerializeField] private float currentValue;

    private float multiplier = 30f;
    private bool _disableToggleEvent;

    //Constructors

    public Slider Slider
    {
        get { return _slider; }
    }

    public string VolumeParameter
    {
        get { return _volumeParameter; }
    }

    private void Awake()
    {
        _slider.onValueChanged.AddListener(HandleSliderValueChanged);
        _toggle.onValueChanged.AddListener(HandleToggleValueChanged);

        _toggle_Text = _toggle.transform.Find("Label").GetComponent<TMP_Text>();
        volume_Percentage = _slider.transform.Find("PercentageTxt").GetComponent<TMP_Text>();
    }

    private void Start()
    {
        _slider.value = PlayerPrefs.GetFloat(_volumeParameter, _slider.value);
    }

    private void HandleToggleValueChanged(bool mute)
    {
        if (_disableToggleEvent)
            return;

        if (mute)
        {
            _slider.value = _slider.minValue;
        }
        else
        {
            _slider.value = 0.8f;
        }
    }

    private void HandleSliderValueChanged(float value)
    {
        _mixer.SetFloat(_volumeParameter, Mathf.Log10(value) * multiplier);
        _disableToggleEvent = true;
        _toggle.SetIsOnWithoutNotify(!(_slider.value > _slider.minValue));
        volume_Percentage.text = Mathf.RoundToInt(value * 100) + "%";
        if (_slider.value == _slider.minValue)
        {
            _toggle_Text.text = " MUTED";
        }
        if (_slider.value > _slider.minValue)
        {
            _toggle_Text.text = " On";
        }
        _disableToggleEvent = false;
    }

    public void OnDisable()
    {
        PlayerPrefs.SetFloat(_volumeParameter, _slider.value);
    }
}
