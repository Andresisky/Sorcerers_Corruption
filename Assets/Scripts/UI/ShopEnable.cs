using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopEnable : MonoBehaviour
{
    AudioSource audioSource;

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void OnEnable()
    {
        audioSource.PlayOneShot(audioSource.clip);
    }
}
