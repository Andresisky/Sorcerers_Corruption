using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeScreenshot : MonoBehaviour
{

    void Update()
    {
        if (PlayerInputs.Instance.Screenshot())
        {
            ScreenCapture.CaptureScreenshot("GameScreenshot.png");
        }
    }
}
