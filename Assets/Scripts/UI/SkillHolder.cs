using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SkillHolder : MonoBehaviour
{
    public Image icon;
    public TMP_Text magityCost;
    public TMP_Text cooldownTime;
}
