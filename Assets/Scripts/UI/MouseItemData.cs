using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class MouseItemData : MonoBehaviour
{
    public Image itemSprite;
    public TextMeshProUGUI itemCount;
    public TextMeshProUGUI itemName;

    public InventorySlot AssignedInventorySlot;

    private void Awake()
    {
        itemSprite.color = Color.clear;
        itemCount.text = "";
        itemName.text = "";
    }

    public void UpdateMouseSlot(InventorySlot invSlot)  
    {
        var tempColor = itemSprite.color;
        tempColor = Color.white;
        tempColor.a = 0.7f;
        AssignedInventorySlot.AssignItem(invSlot);
        itemSprite.sprite = invSlot.ItemData.itemIconPNG;
        itemCount.text = invSlot.StackSize.ToString();
        itemSprite.color = tempColor;
    }

    private void Update()
    {
        //  TODO: Add controller support.

        if (AssignedInventorySlot.ItemData != null) //If has item follow the mouse position.
        {
            transform.position = Mouse.current.position.ReadValue();

            if (Mouse.current.leftButton.wasPressedThisFrame && !IsPointerOverUIObject())
            {
                ClearSlot();
                //TODO: Drop item on the ground? or do something when click outside the inventory.
            }

        }
    }

    public void ClearSlot()
    {
        AssignedInventorySlot.ClearSlot();
        itemCount.text = "";
        itemSprite.color = Color.clear;
        itemSprite.sprite = null;
    }

    public static bool IsPointerOverUIObject()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = Mouse.current.position.ReadValue();
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }

}
