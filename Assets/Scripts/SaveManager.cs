using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveManager
{
    public static void SavePlayerData(PlayerSystem player)
    {
        PlayerData playerData = new PlayerData(player);
        string dataPath = Application.persistentDataPath + "/playerSave.corruption";
        FileStream fileStream = new FileStream(dataPath, FileMode.Create);
        BinaryFormatter binaryFormatter = new BinaryFormatter();
        binaryFormatter.Serialize(fileStream, playerData);
        Debug.Log("Data saved on: " + dataPath);
        fileStream.Close();
    }

    /*public static void SaveInventoryData(InventoryHolder inventory)
    {
        InventoryData inventoryData = new InventoryData(inventory);
        string dataPath = Application.persistentDataPath + "/inventorySave.corruption";
        FileStream fileStream = new FileStream(dataPath, FileMode.Create);
        BinaryFormatter binaryFormatter = new BinaryFormatter();
        binaryFormatter.Serialize(fileStream, inventoryData);
        fileStream.Close();
    }*/

    public static PlayerData LoadPlayerData()
    {
        string dataPath = Application.persistentDataPath + "/playerSave.corruption";

        if (File.Exists(dataPath))
        {
            FileStream fileStream = new FileStream(dataPath, FileMode.Open);
            BinaryFormatter binFormatter = new BinaryFormatter();
            PlayerData playerData = (PlayerData) binFormatter.Deserialize(fileStream);
            fileStream.Close();
            return playerData;
        }
        else
        {
            Debug.LogError("No saved files found");
            return null;
        }
    }

    /*public static InventoryData LoadInventoryData()
    {
        string dataPath = Application.persistentDataPath + "/inventorySave.corruption";

        if (File.Exists(dataPath))
        {
            FileStream fileStream = new FileStream(dataPath, FileMode.Open);
            BinaryFormatter binFormatter = new BinaryFormatter();
            InventoryData inventory = (InventoryData)binFormatter.Deserialize(fileStream);
            fileStream.Close();
            return inventory;
        }
        else
        {
            Debug.LogError("No saved files found");
            return null;
        }
    }*/
}
