using UnityEngine;
using UnityEngine.VFX;

public class SwordController : MonoBehaviour
{
    private AudioSource audioSource;
    public AudioClip[] swordWoosh;
    [SerializeField] private Material swordMat;
    [SerializeField] private Material deadlyEdgeMat;
    public VisualEffect vfx_edge;

    public Material DeadlyEdgeMat
    {
        get { return deadlyEdgeMat; }
        set { deadlyEdgeMat = value; }
    }
    public Material SwordMat
    {
        get { return swordMat; }
        set { swordMat = value; }
    }

    public VisualEffect VfxEdge
    {
        get { return vfx_edge; }
        set { vfx_edge = value; }
    }

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void SwordWoosh()
    {
        audioSource.PlayOneShot(swordWoosh[Random.Range(0, swordWoosh.Length)]);
    }
}
