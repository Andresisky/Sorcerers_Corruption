using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Skills : MonoBehaviour
{
    [Header("")]
    public SkillsSO[] skill;
    private PlayerInputs playerInputs;
    private Animator anim;
    AudioSource audioSource;

    //DeadlyEdge
    [Header("Deadly Edge")]
    [SerializeField] private Image deadlyEdgeCooldown;
    [SerializeField] private TMP_Text deadlyEdgeCooldownText;
    [SerializeField] private AudioClip deadlyEdgeSFX;
    private WeaponManager weaponManager;


    //FoxosBlessing
    [Header("Foxos Blessing")]
    [SerializeField] private int shieldHealth = 30;
    [SerializeField] private Image foxosCooldown;
    [SerializeField] private TMP_Text foxosCooldownText;
    [SerializeField] private AudioClip foxos_cast;
    [SerializeField] private AudioClip[] foxos_hit;
    [SerializeField] private AudioClip foxos_destroy;
    [SerializeField] private Transform foxosHolder;
    [SerializeField] private ParticleSystem foxosVFX;
    private PlayerSystem playerSystem;

    //Ultrasound
    [Header("Ultrasound")]
    public GameObject ultrasoundObject;
    [SerializeField] private Image ultrasoundCooldown;
    [SerializeField] private AudioClip ultrasound_charging;
    [SerializeField] private AudioClip ultrasound_explosion;
    [SerializeField] private TMP_Text ultrasoundCooldownText;
    [SerializeField] private ParticleSystem ultrasoundVFX;
    
    //SoulArrow
    [Header("SoulArrow")]
    [SerializeField] private GameObject arrow;
    [SerializeField] private Transform arrowSpawnpoint;
    [SerializeField] private Image soulArrowCooldown;
    [SerializeField] private TMP_Text soulArrowCooldownText;
    [SerializeField] private AudioClip bowCharge;
    [SerializeField] private AudioClip bowShoot;

    #region Singleton

    public static Skills s_Instance;

    #endregion

    private void Awake()
    {
        if (s_Instance)
        {
            Destroy(this.gameObject);
        }
        else
        {
            s_Instance = this;
        }

        anim = GetComponent<Animator>();
        playerSystem = FindObjectOfType<PlayerSystem>();
        weaponManager = GetComponent<WeaponManager>();
        audioSource = GetComponent<AudioSource>();
        ultrasoundVFX.Stop();
    }
    private void Start()
    {
        ultrasoundObject.SetActive(false);
        playerInputs = PlayerInputs.Instance;
        SetUIOff();
        SetSkillsOff();
    }

    private void SetUIOff()
    {
        deadlyEdgeCooldownText.gameObject.SetActive(false);
        deadlyEdgeCooldown.fillAmount = 0.0f;
        foxosCooldownText.gameObject.SetActive(false);
        foxosCooldown.fillAmount = 0.0f;
        ultrasoundCooldownText.gameObject.SetActive(false);
        ultrasoundCooldown.fillAmount = 0.0f;
        soulArrowCooldownText.gameObject.SetActive(false);
        soulArrowCooldown.fillAmount = 0.0f;
    }

    private void SetSkillsOff()
    {
        for (int i = 0; i < skill.Length; i++)
        {
            skill[i].CooldownTimer = 0;
            skill[i].BuffDurationTimer = 0;
            skill[i].IsActive = false;
            skill[i].BuffActive = false;
        }
    }

    private void Update()
    {
        CheckUIUpdates();
        if (!playerSystem.playerDead)
        {
            weaponManager.deadlyEdge_Enabled = skill[0].BuffActive;
            if (playerInputs.DeadlyEdgeActivated() && !skill[0].IsActive)
            {
                if (playerSystem.magity >= skill[0].magityCost)
                {
                    //DeadlyEdge();
                    anim.SetInteger("Skill", 1);
                    //anim.Play("DeadlyEdge");
                }
                else
                {
                    Debug.Log("No Magity Suficiente");
                }

            }
            if (playerInputs.FoxosBlessingActivated() && !skill[1].IsActive)
            {
                if (playerSystem.magity >= skill[1].magityCost)
                {
                    //FoxosBlessing();
                    anim.SetInteger("Skill", 2);
                }
                else
                {
                    Debug.Log("No Magity Suficiente");
                }
            }
            if (playerInputs.UltrasoundActivated() && !skill[2].IsActive)
            {
                if (playerSystem.magity >= skill[2].magityCost)
                {
                    anim.SetInteger("Skill", 3);
                    //Ultrasound();
                }
                else
                {
                    Debug.Log("No Magity Suficiente");
                }
            }
            if (playerInputs.SoulArrowActivated() && !skill[3].IsActive)
            {
                if (playerSystem.magity >= skill[3].magityCost)
                {
                    SoulArrow();
                }
                else
                {
                    Debug.Log("No Magity Suficiente");
                }
            }
        }    
    }

    void CheckUIUpdates()
    {

        //Deadly Edge
        if (skill[0].CooldownTimer < 0.0f)
        {
            skill[0].IsActive = false;
            deadlyEdgeCooldownText.gameObject.SetActive(false);
            deadlyEdgeCooldown.fillAmount = 0.0f;
        }
        else
        {
            skill[0].CooldownTimer -= Time.deltaTime;
            
            deadlyEdgeCooldownText.text = Mathf.RoundToInt(skill[0].CooldownTimer).ToString();
            deadlyEdgeCooldown.fillAmount = skill[0].CooldownTimer / skill[0].CooldownTime;
        }

        if (skill[0].BuffDurationTimer < 0.0f)
        {
            skill[0].BuffActive = false;
        }
        else
        {
            skill[0].BuffDurationTimer -= Time.deltaTime;
        }

        //
        //Foxos

        if (skill[1].CooldownTimer < 0.0f)
        {
            skill[1].IsActive = false;
            foxosCooldownText.gameObject.SetActive(false);
            foxosCooldown.fillAmount = 0.0f;
        }
        else
        {
            skill[1].CooldownTimer -= Time.deltaTime;

            foxosCooldownText.text = Mathf.RoundToInt(skill[1].CooldownTimer).ToString();
            foxosCooldown.fillAmount = skill[1].CooldownTimer / skill[1].CooldownTime;
        }

        //Ultrasound
        if (skill[2].CooldownTimer < 0.0f)
        {
            skill[2].IsActive = false;
            ultrasoundCooldownText.gameObject.SetActive(false);
            ultrasoundCooldown.fillAmount = 0.0f;
        }
        else
        {
            skill[2].CooldownTimer -= Time.deltaTime;

            ultrasoundCooldownText.text = Mathf.RoundToInt(skill[2].CooldownTimer).ToString();
            ultrasoundCooldown.fillAmount = skill[2].CooldownTimer / skill[2].CooldownTime;
        }

        //SoulArrow
        if (skill[3].CooldownTimer < 0.0f)
        {
            skill[3].IsActive = false;
            soulArrowCooldownText.gameObject.SetActive(false);
            soulArrowCooldown.fillAmount = 0.0f;
        }
        else
        {
            skill[3].CooldownTimer -= Time.deltaTime;

            soulArrowCooldownText.text = Mathf.RoundToInt(skill[3].CooldownTimer).ToString();
            soulArrowCooldown.fillAmount = skill[3].CooldownTimer / skill[3].CooldownTime;
        }
    }

    public void DeadlyEdge()
    {
        deadlyEdgeCooldownText.gameObject.SetActive(true);
        audioSource.PlayOneShot(deadlyEdgeSFX);
        skill[0].CooldownTimer = skill[0].CooldownTime;
        skill[0].BuffDurationTimer = skill[0].BuffDuration;
        skill[0].BuffActive = true;
        skill[0].IsActive = true;
        playerSystem.ChangeMagity(skill[0].magityCost);
        anim.SetInteger("Skill", 0);
    }

    public void FoxosBlessing()
    {
        foxosCooldownText.gameObject.SetActive(true);
        skill[1].CooldownTimer = skill[1].CooldownTime;
        skill[1].IsActive = true;
        playerSystem.ShieldHealth = shieldHealth;
        playerSystem.foxosActived = true;
        playerSystem.ChangeMagity(skill[1].magityCost);
        anim.SetInteger("Skill", 0);
    }

    public void Ultrasound()
    {
        audioSource.PlayOneShot(ultrasound_explosion);
        ultrasoundCooldownText.gameObject.SetActive(true);
        skill[2].CooldownTimer = skill[2].CooldownTime;
        skill[2].IsActive = true;
        ultrasoundObject.SetActive(true);
        playerSystem.ChangeMagity(skill[2].magityCost);
        anim.SetInteger("Skill", 0);
    } 

    public void ChargingUltrasound()
    {
        ultrasoundVFX.Play();
        audioSource.PlayOneShot(ultrasound_charging);
    }

    public void ChargingFoxos()
    {
        audioSource.PlayOneShot(foxos_cast);
        //foxosVFX.Play();
        Instantiate(foxosVFX, foxosHolder);
    }

    public void SoulArrow()
    {
        audioSource.PlayOneShot(bowShoot);
        soulArrowCooldownText.gameObject.SetActive(true);
        skill[3].CooldownTimer = skill[3].CooldownTime;
        skill[3].IsActive = true;
        playerSystem.ChangeMagity(skill[3].magityCost);
        Instantiate(arrow, arrowSpawnpoint.position, arrowSpawnpoint.rotation);

    }

}
