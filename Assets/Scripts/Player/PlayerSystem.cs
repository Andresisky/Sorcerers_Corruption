using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Cinemachine;

public class PlayerSystem : MonoBehaviour
{
    private WeaponManager weaponManager;
    private PlayerMovement playerMovement;
    private PlayerInputs playerInputs;

    public int hearts = 3;
    private readonly float lerpSpeed = 5f;
    private GameObject vcam;
    private CinemachineVirtualCamera fpsCam;
    public Transform legs;

    //HP
    [Header("Health Points")]
    public float maxHP = 100;
    private float currentHP;
    private float previousHP;
    public float hp;
    private bool HPshouldLerp = false;

    //Magity
    [Header("Magity")]
    public float maxMagity = 100;
    private float currentMagity;
    private float previousMagity;
    public float magity;
    private bool MagityshouldLerp = false;

    [HideInInspector] public bool isBlocking;

    public int money = 0;
    [SerializeField] private TMP_Text moneyCount;

    public float damage;
    private bool tookDamage = false;
    [HideInInspector] public int currentDamage = 0;

    public AudioClip[] hurtingSounds;
    public AudioClip[] deathSounds;
    public bool playerDead = false;
    private bool gameOver = false;


    //FoxosBlessing
    [SerializeField] private int shieldHealth = 0;
    public bool foxosActived;

    //UI Stuff
    [Header("HUD")]
    //private SliderBar sliderBar;
    [SerializeField] private Image hpBar;
    [SerializeField] private Image magityBar;
    [SerializeField] private GameObject inventoryUI;
    public GameObject playerHUD;
    [SerializeField] bool inventoryOpen = false;

    #region Properties

    public int ShieldHealth
    {
        get { return shieldHealth; }
        set { shieldHealth = value; }
    }

    #endregion

    private void Awake()
    {
        currentHP = previousHP = hp = maxHP;
        currentMagity = previousMagity = magity = maxMagity;
        gameOver = false;
        playerDead = false;
        tookDamage = false;

        playerInputs = GetComponent<PlayerInputs>();
        playerMovement = GetComponent<PlayerMovement>();
        weaponManager = GameObject.Find("Arms").GetComponent<WeaponManager>();
        vcam = GameObject.Find("CM vcam1");
        fpsCam = vcam.GetComponent<CinemachineVirtualCamera>();
        
    }

    void Update()
    {
        damage = weaponManager.damage;
        if (PlayerInputs.Instance.Inventory() && !playerDead)
        {
            OpenInventory();
        }

        isBlocking = weaponManager.isBlocking;
        moneyCount.text = money.ToString();
        if (money < 0)
        {
            money = 0;
        }
        if (playerDead)
        {
            DisableMovement();
            DisableHUD();
        }
        CheckLife();
        CheckHP();
        CheckMagity();
        IncreaseGradually();
    }

    void CheckLife()
    {
        if (hearts <= 0 && playerDead == false)
        {
            gameOver = true;
            GameOver();
        }
    }

    void GameOver()
    {
        playerDead = true;
        GameObject audio;
        AudioSource audioSource;
        audio = GameObject.Find("Player/AudioManager/PlayerSFX");
        audioSource = audio.GetComponent<AudioSource>();
        DisableMovement();

        audioSource.clip = deathSounds[Random.Range(0, deathSounds.Length)];
        audioSource.Play();
        if (gameOver == true)
        {
            Debug.Log("Game Over Screen");
        }
    }
    public void TakeDamage(int damage)
    {
        ChangeHealth(damage);
        if (!foxosActived)
        {
            int randomSound = Random.Range(0, hurtingSounds.Length + 20);
            GameObject audio;
            AudioSource audioSource;
            audio = GameObject.Find("AudioManager/PlayerSFX");
            audioSource = audio.GetComponent<AudioSource>();

            tookDamage = true;
            currentHP -= damage;

            if (damage > 10 || randomSound < 10)
            {
                audioSource.clip = hurtingSounds[Random.Range(0, hurtingSounds.Length)];
                audioSource.Play();
            }
        }
        else
        {
            shieldHealth -= damage;
        }
    }

    #region HP
    void CheckHP()
    {
        if (HPshouldLerp)
        {
            LerpDamage();
        }

        if (tookDamage == true)
        {
            tookDamage = false;
        }

        if (currentHP <= 0 && playerDead == false)
        {
            hearts--;
            GameOver();
        }
        if (shieldHealth <= 0)
        {
            foxosActived = false;
        }

        currentDamage = 0;
    }

    public void ChangeHealth(float amount)
    {
        previousHP = currentHP;
        hp = currentHP - amount;
        hp = Mathf.Clamp(hp, 0, maxHP);
        HPshouldLerp = true;
    }

    float t = 0;
    void LerpDamage()
    {
        t += lerpSpeed * Time.deltaTime;
        currentHP = Mathf.Lerp(previousHP, hp, t);
        hpBar.fillAmount = currentHP / maxHP;

        if (currentHP == hp)
        {
            HPshouldLerp = false;
            t = 0;
        }
    }

    #endregion

    #region Magity
    void CheckMagity()
    {
        if (MagityshouldLerp)
        {
            LerpMagity();
        }
    }

    public void ChangeMagity(float amount)
    {
        previousMagity = currentMagity;
        magity = currentMagity - amount;
        magity = Mathf.Clamp(magity, 0, maxMagity);
        MagityshouldLerp = true;
    }
    float t2 = 0;
    void LerpMagity()
    {
        t2 += lerpSpeed * Time.deltaTime;
        currentMagity = Mathf.Lerp(previousMagity, magity, t2);
        magityBar.fillAmount = currentMagity / maxMagity;

        if (currentMagity == magity)
        {
            MagityshouldLerp = false;
            t2 = 0;
        }
    }
    #endregion

    void IncreaseGradually()
    {
        //ChangeMagity(-(0.5f * Time.deltaTime));
        //ChangeHealth(-(0.5f * Time.deltaTime));
    }


    void OpenInventory()
    {
        if (!inventoryOpen)
        {
            //InventoryManager.Instance.ListItems();
            inventoryUI.SetActive(true);
            DisableMovement();
            inventoryOpen = true;
            Time.timeScale = 0.07f;

            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
        else
        {
            inventoryUI.SetActive(false);
            EnableMovement();
            inventoryOpen = false;
            Time.timeScale = 1.0f;


            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }

    public void UpdateHUD()
    {
        hpBar.fillAmount = hp / maxHP;
        magityBar.fillAmount = magity / maxMagity;
    }

    public void DisableHUD()
    {
        playerHUD.SetActive(false);
    }

    public void EnableHUD()
    {
        playerHUD.SetActive(true);
    }

    public void DisableMovement()
    {
        playerMovement.enabled = false;
        weaponManager.enabled = false;
        fpsCam.enabled = false;
    }

    public void EnableMovement()
    {
        playerMovement.enabled = true;
        weaponManager.enabled = true;
        fpsCam.enabled = true;
    }
}
