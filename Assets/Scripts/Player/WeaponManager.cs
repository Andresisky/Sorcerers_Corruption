using System.Collections;
using UnityEngine;

public class WeaponManager : MonoBehaviour
{
    public float damage = 5f;
    public float range = 100f;
    private Animator anim;
    public Camera fpsCam;

    //Combo Variables
    [SerializeField] private int pressNumber;
    [SerializeField] private bool canPress;


    //Shield
    public bool isBlocking = false;

    //Sword
    private SwordController sword;
    public bool deadlyEdge_Enabled = false;
    [SerializeField] private MeshRenderer swordMesh;
    public ParticleSystem trails;
    private Skills deadlyEdgeSkill;


    RaycastHit hit;

    private void Awake()
    {
        anim = GetComponent<Animator>();
        sword = GetComponentInChildren<SwordController>();
        pressNumber = 0;
        canPress = true;
        trails.Stop();
    }

    private void Start()
    {
        deadlyEdgeSkill = Skills.s_Instance;
    }

    void Update()
    {
        Debug.DrawRay(fpsCam.transform.position, fpsCam.transform.forward, Color.blue);

        if (Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit, range))
        {
            if (hit.distance < 2)
            {
                IInteractable interactable = hit.collider.GetComponent<IInteractable>();
                if (interactable != null)
                {
                    if (PlayerInputs.Instance.Interact())
                    {
                        interactable.Interact();
                    }
                }
            }

        }


        if (!deadlyEdge_Enabled)
        {
            swordMesh.material = sword.SwordMat;
            if (deadlyEdgeSkill.skill[0].BuffDurationTimer <= 0 && deadlyEdgeSkill.skill[0].IsActive)
            {
                StartCoroutine(CheckVFX());
            }
        }
        if (deadlyEdge_Enabled)
        {
            sword.vfx_edge.gameObject.SetActive(true);
            swordMesh.material = sword.DeadlyEdgeMat;
            sword.vfx_edge.enabled = true;
        }


        if (PlayerInputs.Instance.Attack())
        {
            if (canPress && pressNumber < 3)
            {
                pressNumber++;
                anim.SetInteger("Attack", pressNumber);
                canPress = false;
            }
        }
        if (isBlocking)
        {
            ShieldBlock();
            anim.SetBool("isBlocking", true);
        }
        else
        {
            StopBlock();
        }


        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Idle_Armed") && pressNumber == 0)
        {
            canPress = true;
        }
    }

    public void Attack()
    {
        if (Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit, range))
        {
            if (!deadlyEdge_Enabled)
            {
                if (hit.distance <= 5)
                {

                    //

                    IDamageable damageable = hit.collider.GetComponent<IDamageable>();
                    if (damageable != null)
                    {
                        damageable.Damage(damage);
                    }

                    //

                }
            }
            else
            {
                //Deadly Edge Code Here
                if (hit.distance <= 5)
                {

                    //

                    IDamageable damageable = hit.collider.GetComponent<IDamageable>();
                    if (damageable != null)
                    {
                        damageable.Damage(damage * 2);
                    }

                    //

                }
            }
        }
    }

    public void VerifyCombo()
    {
        if (canPress)
        {
            canPress = false;
            pressNumber = 0;
            anim.SetInteger("Attack", pressNumber);
        }
    }

    public void CanPressTrue()
    {
        canPress = true;
    }

    public void IsShielded()
    {
        isBlocking = true;
    }

    public void NotShielded()
    {
        isBlocking = false;
    }


    public void ShieldBlock()
    {
        anim.SetBool("isBlocking", true);
    }

    public void StopBlock()
    {
        anim.SetBool("isBlocking", false);
    }

    //Sword
    private IEnumerator CheckVFX()
    {
        sword.vfx_edge.Stop();
        yield return new WaitForSeconds(2);
        sword.vfx_edge.gameObject.SetActive(false);
        yield return null;
    }

    public void SwordWoosh()
    {
        sword.SwordWoosh();
    }

    public void StartTrails()
    {
        trails.Play();
    }

    public void EndTrails()
    {
        trails.Stop();
    }

}
