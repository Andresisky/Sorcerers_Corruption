using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class InventoryManager
{
    [SerializeField] private List<InventorySlot> inventorySlots;
    public List<InventorySlot> InventorySlots => inventorySlots;
    public int InventorySize => InventorySlots.Count;

    public UnityAction<InventorySlot> OnInventorySlotChanged;

    public InventoryManager(int size)   // Constructor that sets the amount of slots
    {
        inventorySlots = new List<InventorySlot>(size);

        for (int i = 0; i < size; i++)
        {
            inventorySlots.Add(new InventorySlot());
        }
    }

    public bool AddToInventory(ItemsSO itemToAdd, int amountToAdd)
    {
        if (ContainsItem(itemToAdd, out List<InventorySlot> invSlot)) // Check if item exists in inventory
        {
            foreach (var slot in invSlot)
            {
                if (slot.EnoughRoomLeftInStack(amountToAdd))
                {
                    slot.AddToStack(amountToAdd);
                    OnInventorySlotChanged?.Invoke(slot);
                    return true;
                }
            }
        }


        if (HasFreeSlot(out InventorySlot freeSlot)) // Gets the first available Slot
        {
            if (freeSlot.EnoughRoomLeftInStack(amountToAdd))
            {
                freeSlot.UpdateInventorySlot(itemToAdd, amountToAdd);
                OnInventorySlotChanged?.Invoke(freeSlot);
                return true;
            }

            // Add implementation to only take what can fill the stack, and check for antoher free slot to put the remainder in.
        }
        else
        {
            Debug.Log("No free spaces on Inventory!");
        }

        return false;
    }

    public bool RemoveFromInventory(ItemsSO itemtoRemove, int amountToRemove)
    {
        if (ContainsItem(itemtoRemove, out List<InventorySlot> invSlot)) // Check if item exists in inventory
        {
            foreach (var slot in invSlot)
            {
                slot.RemoveFromStack(amountToRemove);
                OnInventorySlotChanged?.Invoke(slot);
                if (slot.StackSize <= 0)
                {
                    slot.ClearSlot();
                }
                return true;
            }
        }

        return false;
    }

    public bool ContainsItem(ItemsSO itemToAdd, out List<InventorySlot> invSlot) // Do any of our slots have the item to add in them?
    {
        invSlot = InventorySlots.Where(i => i.ItemData == itemToAdd).ToList(); // If they do, get a list of all of them.
        return invSlot == null ? false : true;  // If they do return true, if not return false.
    }

    public bool HasFreeSlot(out InventorySlot freeSlot)
    {
        freeSlot = InventorySlots.FirstOrDefault(i => i.ItemData == null);  // Get the first free slot
        return freeSlot == null ? false : true;
    }
}
