using UnityEngine;
using Cinemachine;

[RequireComponent(typeof(CharacterController))]
public class PlayerMovement : MonoBehaviour
{
    private CharacterController controller;
    [SerializeField] private CinemachineVirtualCamera vcam;

    public Animator anim;
    public float playerSpeed = 3.0f;
    [SerializeField] private float jumpHeight = 0.7f;
    private readonly float gravityValue = -9.81f;
    private Vector3 playerVelocity;
    private bool isGrounded;

    [SerializeField] bool isRunning;

    //Input
    Vector2 currentMovement;



    public void ReceiveInput(Vector2 input)
    {
        currentMovement = input;
    }

    private Transform cameraTransform;

    private void Start()
    {
        controller = GetComponent<CharacterController>();
        cameraTransform = Camera.main.transform;
    }

    void Update()
    {
        anim.SetBool("isRunning", false);
        anim.SetBool("isWalking", false);
        isGrounded = controller.isGrounded;

        if (isGrounded && playerVelocity.y < 0)
        {
            playerVelocity.y = 0f;
        }

        Vector2 movement = currentMovement;
        Vector3 move = new Vector3(movement.x, 0f, movement.y);
        move = cameraTransform.forward * move.z + cameraTransform.right * move.x;
        move.y = 0f;
        controller.Move(move * Time.deltaTime * playerSpeed);

        if (currentMovement.magnitude > 0 )
        {
            if (playerSpeed <= 3)
            {
                anim.SetBool("isWalking", true);
                vcam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_PivotOffset.z = 2f;
                vcam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain = 1f;
                vcam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_FrequencyGain = 1.5f;

            }
            if (playerSpeed > 4)
            {
                anim.SetBool("isRunning", true);
                vcam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_PivotOffset.z = 1f;
                vcam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain = 2f;
                vcam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_FrequencyGain = 3f;
            }
        }
        else
        {
            vcam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_PivotOffset.z = 0.1f;
            vcam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain = 0.2f;
            vcam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_FrequencyGain = 1f;
        }

        // Jump

        if (PlayerInputs.Instance.Jump() && isGrounded)
        {
            playerVelocity.y += Mathf.Sqrt(jumpHeight * -3.0f * gravityValue);
        }

        if (isRunning)
        {
            playerSpeed = 5f;
        }
        else
        {
            playerSpeed = 3f;
        }

        playerVelocity.y += gravityValue * Time.deltaTime;
        controller.Move(playerVelocity * Time.deltaTime);
    }

    public void OnSprint()
    {
        isRunning = true;
    }

    public void NotSprint()
    {
        isRunning = false;
    }    
}