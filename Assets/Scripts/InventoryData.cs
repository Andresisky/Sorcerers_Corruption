[System.Serializable]
public class InventoryData
{
    public int inventorySize;
    public int pocketSize;
    public InventoryManager inventoryManager;
    public InventorySlot[] slot;
    public ItemsSO itemData;
    public int itemStack;

    public InventoryData(InventoryHolder inventory)
    {

    }
}
