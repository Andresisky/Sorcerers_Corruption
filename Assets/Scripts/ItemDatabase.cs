using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDatabase : MonoBehaviour
{
    private PlayerSystem playerSystem;
    private WeaponManager weaponManager;
    private AudioSource audioSource;

    [Header("Health Potion")]
    public ParticleSystem healthPotionVFX;

    [Header("Magity Potion")]
    public ParticleSystem magityPotionVFX;
    private bool magityIsActive;
    private float magityTimer;


    private bool bottledFuryisActive;
    private bool valleysBlessingisActive;

    float damage;

    ItemsSO item;

    private static ItemDatabase _instance;
    public static ItemDatabase Instance
    {
        get
        {
            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        playerSystem = FindObjectOfType<PlayerSystem>();
        weaponManager = FindObjectOfType<WeaponManager>();
        audioSource = GetComponent<AudioSource>();

    }

    private void Update()
    {
        if (magityIsActive)
        {
            if (magityTimer < 0.0f)
            {
                magityTimer = 0.0f;
                magityIsActive = false;
            }
            else
            {
                magityTimer -= Time.deltaTime;
                playerSystem.ChangeMagity(-(5 * Time.deltaTime));
            }
        }
    }

    public void HealthPotion()
    {
        playerSystem.ChangeHealth(-10f);
        Instantiate(healthPotionVFX, playerSystem.legs);
    }

    public void MagityPotion(ItemsSO magityItem)
    {
        playerSystem.ChangeMagity(-(5 * Time.deltaTime));
        magityTimer = magityItem.BuffTimer;
        Instantiate(magityPotionVFX, playerSystem.legs);
        magityIsActive = true;
    }

    public void ValleyPotion()
    {
        playerSystem.ChangeHealth(-playerSystem.maxHP);
    }

    public void DrinkSFX(ItemsSO item)
    {
        audioSource.PlayOneShot(item.Sound);
    }

}

