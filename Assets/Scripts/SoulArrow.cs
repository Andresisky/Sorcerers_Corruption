using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoulArrow : MonoBehaviour
{
    private PlayerSystem player;
    [SerializeField] private Vector3 arrowRotation;
    [SerializeField] private float speed = 0.2f;
    private Rigidbody rb;

    [SerializeField] AudioSource audioSource;
    [SerializeField] AudioClip arrowHit;
    [SerializeField] AudioClip arrowDestroy;

    private float currentTime;
    private float startingTime = 8;
    bool destroy = false;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        player = FindObjectOfType<PlayerSystem>();
        audioSource = GetComponent<AudioSource>();
        currentTime = startingTime;
    }
    void Update()
    {
        transform.Rotate(arrowRotation * Time.deltaTime);
        rb.AddForce(transform.forward * speed * Time.deltaTime);
        currentTime -= 1 * Time.deltaTime;
        if (currentTime <= 0)
        {
            gameObject.GetComponent<Collider>().enabled = false;
            foreach (Transform child in transform)
            {
                child.gameObject.SetActive(false);
            }
            if (!destroy)
            {
                audioSource.PlayOneShot(arrowDestroy);
                destroy = true;
            }
            
            Destroy(gameObject, 1f);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            audioSource.PlayOneShot(arrowHit);
            player.ChangeMagity(-7f);
        }
    }
}
