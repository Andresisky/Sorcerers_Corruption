using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldGenerator : MonoBehaviour
{
    [SerializeField] private int minLoot = 100;
    [SerializeField] private int maxLoot = 200;
    /*[HideInInspector]*/ public int moneyLoot;

    private void Awake()
    {
        moneyLoot = Random.Range(minLoot, maxLoot);
    }
}
