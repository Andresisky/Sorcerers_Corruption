using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Items : MonoBehaviour
{
    [SerializeField] private ItemsSO[] items;
    private PlayerInputs playerInputs;
    private PlayerSystem playerSystem;
    [SerializeField] private AudioSource audioSource;

    //Health Potion
    [SerializeField] private int healthPotionQuantity;
    [SerializeField] private bool healthPotionIsEquipped;

    //Magity Potion
    [SerializeField] private int magityPotionQuantity;
    [SerializeField] private bool magityPotionIsEquipped;
    [SerializeField] private bool magityIsActive;
    [SerializeField] private float number = 0;

    //New Chance

    //

    //Greeds Amulet
    public bool greedsAmulet;

    //

    //Bottled Fury
    [SerializeField] private bool bottledFuryisActive;
    private WeaponManager weaponManager;
    private int bottledFuryQuantity;
    float damage;

    //Valleys Blessing
    [SerializeField] private bool valleysBlessingisActive;
    private int valleysBlessingQuantity;

    private void Start()
    {
        playerInputs = PlayerInputs.Instance;
        weaponManager = GameObject.Find("Arms").GetComponent<WeaponManager>();
        playerSystem = GetComponent<PlayerSystem>();
        damage = weaponManager.damage;
        healthPotionQuantity = items[0].MaxStackSize;
        magityPotionQuantity = items[1].MaxStackSize;
        bottledFuryQuantity = items[2].MaxStackSize;
        valleysBlessingQuantity = items[3].MaxStackSize;
        for (int i = 0; i < items.Length; i++)
        {
            items[i].BuffTimer = items[i].BuffDuration;
        }
    }

    private void Update()
    {
        TimeChecker();
        if (playerInputs.Item1())
        {
            ValleysBlessing();
        }
        if (playerInputs.Item2())
        {
            MagityPotion();
        }
    }


    void HealthPotion()
    {
        if (healthPotionQuantity > 0)
        {
            playerSystem.ChangeHealth(-10f);
            audioSource.PlayOneShot(items[0].Sound);
            healthPotionQuantity--;
        }
    }

    void MagityPotion()
    {
        if (magityPotionQuantity > 0)
        {
            number = 0;
            magityIsActive = true;
            items[1].BuffTimer = items[1].BuffDuration;
            audioSource.PlayOneShot(items[1].Sound);
            magityPotionQuantity--;
        }
    }

    void BottledFury()
    {
        if (bottledFuryQuantity > 0)
        {
            number = 0;
            bottledFuryisActive = true;
            items[2].BuffTimer = items[2].BuffDuration;
            weaponManager.damage *= 2;
            bottledFuryQuantity--;
        }
    }

    void ValleysBlessing()
    {
        if (bottledFuryQuantity > 0)
        {
            number = 0;
            valleysBlessingisActive = true;
            items[3].BuffTimer = items[3].BuffDuration;
            playerSystem.hp += 100;
            valleysBlessingQuantity--;
        }
    }

    void NewChance()
    {
        playerSystem.hearts++;
        audioSource.PlayOneShot(items[5].Sound);
    }

    void IncreaseGradually()
    {
        if (magityIsActive)
        {
            playerSystem.ChangeMagity(-(5 * Time.deltaTime));
        }    
        if (valleysBlessingisActive)
        {
            playerSystem.ChangeHealth(-(5 * Time.deltaTime));
        }
    }

    void TimeChecker()
    {
        
        if (magityIsActive)
        {
            IncreaseGradually();
            if (items[1].BuffTimer < 0.0f)
            {
                items[1].BuffTimer = 0.0f;
                magityIsActive = false;
            }
            else
            {
                items[1].BuffTimer -= Time.deltaTime;
                number += Time.deltaTime;
            }
        }
        if (bottledFuryisActive)
        {
            if (items[2].BuffTimer < 0.0f)
            {
                items[2].BuffTimer = 0.0f;
                bottledFuryisActive = false;
            }
            else
            {
                items[2].BuffTimer -= Time.deltaTime;
                number += Time.deltaTime;
            }
        }
        else
        {
            weaponManager.damage = damage;
        }
        if (valleysBlessingisActive)
        {
            IncreaseGradually();
            if (items[3].BuffTimer < 0.0f)
            {
                items[3].BuffTimer = 0.0f;
                valleysBlessingisActive = false;
            }
            else
            {
                items[3].BuffTimer -= Time.deltaTime;
                number += Time.deltaTime;
            }
        }
    }

}
