using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wisp : EnemyChaser
{
    [Header("Wisp Settings")]
    [SerializeField] private Transform bulletSpawnPoint;
    [SerializeField] private GameObject bulletPrefab;

    [SerializeField] ParticleSystem deathParticles;


    protected override void Update()
    {
        base.Update();
        deathParticles.Play();
    }

    protected override void Attack()
    {
        base.Attack();
        if (!AlreadyAttacked)
        {
            //Attack Code here

            if (!IsDead && !Player.GetComponent<PlayerSystem>().playerDead)
            {
                Instantiate(bulletPrefab, bulletSpawnPoint.position, bulletSpawnPoint.rotation);
            }

            //
            AlreadyAttacked = true;
            Invoke(nameof(ResetAttack), TimeBetweenAttacks);
        }
    }

    public override void Death()
    {
        deathParticles.Play();
        DisableParticles();
        Destroy(gameObject, 0.5f);
    }

    private void DisableParticles()
    {
        GameObject GlowingSphere = GameObject.Find(this.gameObject.name + "/System/Glowing Sphere");
        GameObject InnerEffect = GameObject.Find(this.gameObject.name + "/System/InnerEffect");
        GameObject Smoke = GameObject.Find(this.gameObject.name + "/System/Smoke");
        GameObject Electricity = GameObject.Find(this.gameObject.name + "/System/Electricity");
        GameObject Sphere = GameObject.Find(this.gameObject.name + "/System/Sphere");

        GlowingSphere.SetActive(false);
        InnerEffect.SetActive(false);
        Smoke.SetActive(false);
        Electricity.SetActive(false);
        Sphere.SetActive(false);
    }

    private void OnDestroy()
    {
        deathParticles.Play();
    }

}
