using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyChaser : Enemy
{
    #region Variables

    [Header("Chaser Settings")]

    [SerializeField] private float speed;
    [SerializeField] private int enemyDamage;
    [SerializeField] private float timeBetweenAttacks;
    private bool alreadyAttacked;
    //

    public LayerMask whatIsPlayer, whatIsGround;

    //Patroling
    private Vector3 walkPoint;
    bool walkPointSet;
    private float walkPointRange;

    //States
    private bool playerInSightRange, playerInAttackRange;
    [Range(0,20)]
    [SerializeField] private float attackRange;

    #endregion

    #region Properties

    protected float Speed
    {
        get { return speed; }
        set { speed = value; }
    }
    protected bool AlreadyAttacked
    {
        get { return alreadyAttacked; }
        set { alreadyAttacked = value; }
    }
    protected bool PlayerInAttackRange
    { 
        get { return playerInAttackRange; }
    }
    protected int EnemyDamage
    {
        get { return enemyDamage; }
    }
    protected float TimeBetweenAttacks
    {
        get { return timeBetweenAttacks; }
    }
    protected bool PlayerInSightRange
    {
        get { return playerInSightRange; }
    }

    #endregion

    #region Methods

    protected virtual void Start()
    {
        Agent.speed = speed;
    }

    protected override void Update()
    {
        base.Update();
        if (!Player.GetComponent<PlayerSystem>().playerDead)
        {
            if (!IsDead && !IsStunned)
            {
                playerInSightRange = Physics.CheckSphere(transform.position, VisionRadius, whatIsPlayer);
                playerInAttackRange = Physics.CheckSphere(transform.position, attackRange, whatIsPlayer);

                if (!playerInSightRange && !playerInAttackRange) Patroling();
                if (playerInSightRange && !playerInAttackRange) FollowPlayer();
                if (playerInAttackRange && playerInSightRange) Attack();
            }
        }
    }


    protected override void OnDrawGizmosSelected()
    {
        base.OnDrawGizmosSelected();
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, attackRange);
    }

    #endregion

    #region StateMachine

    private void Patroling()
    {
        if (!walkPointSet) SearchWalkPoint();
        if (walkPointSet)
        {
            Agent.SetDestination(walkPoint);
        }
        Vector3 distanceToWalkPoint = transform.position - walkPoint;

        if (distanceToWalkPoint.magnitude < 1f)
        {
            walkPointSet = false;
        }

    }

    private void SearchWalkPoint()
    {
        float randomZ = Random.Range(-walkPointRange, walkPointRange);
        float randomX = Random.Range(-walkPointRange, walkPointRange);

        walkPoint = new Vector3(transform.position.x + randomX, 0, transform.position.z + randomZ);

        if (Physics.Raycast(walkPoint, - transform.up, 20f, whatIsGround))
        {
            walkPointSet = true;
        }
    }

    protected virtual void FollowPlayer()
    {
        Agent.speed = speed;
        if (!IsDead || !IsStunned)
        {
            if (!alreadyAttacked)
            {
                Agent.isStopped = false;
                Agent.SetDestination(Player.transform.position);
            }
        }
    }

    protected virtual void Attack()
    {
        if (!IsDead || !IsStunned)
        {
            Agent.speed = 0;
            var lookPos = Player.transform.position - transform.position;
            float damping = 6f;
            lookPos.y = 0;
            var rotation = Quaternion.LookRotation(lookPos);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * damping);
        }
    }

    protected virtual void GiveDamage()
    {
        Player.GetComponent<PlayerSystem>().hp -= enemyDamage;
    }




    protected virtual void ResetAttack()
    {
        alreadyAttacked = false;
    }

    #endregion
}
