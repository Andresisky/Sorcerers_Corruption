using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

[RequireComponent(typeof(NavMeshAgent), typeof(Collider), typeof(Rigidbody)), SelectionBase]
public class Enemy : MonoBehaviour
{
    #region Variables

    [Header("Enemy Settings")]
    //Components
    private Collider col;
    private Rigidbody rb;
    private NavMeshAgent agent;
    private GameObject player;
    private Transform lootSpawnPoint;
    private GameObject goldSack;
    //private HitCollider hit;

    //Attributes
    public float health = 100;
    private float maxHealth;

    [Range(0, 20)]
    [SerializeField] private float visionRadius = 10f;


    //Booleans
    private bool isDead = false;
    private bool gotHit = false;
    private bool giveLoot = false;
    [SerializeField] private bool isStunned = false;



    #endregion

    #region Properties

    protected float VisionRadius
    {
        get { return visionRadius; }
    }

    protected bool IsStunned
    {
        get { return isStunned; }
    }
    protected GameObject Player
    {
        get
        {
            return player = GameObject.FindGameObjectWithTag("Player");
        }
    }

    protected NavMeshAgent Agent
    {
        get { return agent; }
        set { agent = value; }
    }

    protected bool IsDead
    {
        get { return isDead; }
    }

    protected float Health
    {
        get
        {
            return health;
        }
        set
        {
            health = value;
        }
    }

    protected bool GotHit
    {
        get { return gotHit; }
    }

    #endregion

    #region Methods

    protected virtual void Awake()
    {
        gameObject.tag = "Enemy";
        gameObject.layer = LayerMask.NameToLayer("WhatIsEnemy");
        goldSack = Resources.Load("Prefabs/Gold Sack") as GameObject;
        maxHealth = health;
        lootSpawnPoint = GameObject.Find(this.gameObject.name.ToString() + "/LootSpawnPoint").transform;
        col = GetComponent<Collider>();
        rb = GetComponent<Rigidbody>();
        agent = GetComponent<NavMeshAgent>();
        player = GameObject.FindGameObjectWithTag("Player");
    }

    protected virtual void Update()
    {
        if (health <= 0)
        {
            isDead = true;
            DropLoot();
            //Death();
        }
        if (isStunned)
        {
            StartCoroutine(Stunned());
        }
    }

    protected virtual void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, visionRadius);
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Ultrasound"))
        {
            isStunned = true;
        }
    }

    #endregion

    #region StateMachine

    public virtual void Death()
    {
        Agent.speed = 0;
        Agent.enabled = false;
        Physics.IgnoreCollision(goldSack.GetComponent<Collider>(), col);
        Destroy(gameObject, 0.1f);
    }

    private void DropLoot()
    {
        if (!giveLoot)
        {
            Instantiate(goldSack, lootSpawnPoint.position, Quaternion.identity);
            giveLoot = true;
        }
    }

    /*public virtual void TakeDamage(float amount)
    {
        health -= amount;
        if (health <= 0)
        {
            Death();
        }
    }*/

    public IEnumerator Stunned()
    {
        agent.speed = 0;
        yield return new WaitForSeconds(5);
        isStunned = false;
    }

    #endregion

}
