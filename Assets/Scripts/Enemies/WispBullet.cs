using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WispBullet : MonoBehaviour
{
    private Transform player;
    public float speed;
    [SerializeField] private int damage = 1;
    private bool hit;

    private void Awake()
    {
        hit = false;
        player = GameObject.Find("Player").GetComponent<Transform>();
    }

    private void Update()
    {
        transform.position = Vector3.Lerp(transform.position, player.position, speed * Time.deltaTime);
    }

    public void CheckHit()
    {
        if (player.GetComponent<PlayerSystem>().isBlocking == true)
        {
            GiveDamage(damage);
        }
        if (player.GetComponent<PlayerSystem>().isBlocking == false)
        {
            GiveDamage(0);
        }
    }

    public void GiveDamage(int currentDamage)
    {
        player.GetComponent<PlayerSystem>().TakeDamage(currentDamage);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" && hit == false)
        {
            TurnOffEffects();
            hit = true;
            CheckHit();
            Destroy(gameObject, 1f);
        }
    }
    private void TurnOffEffects()
    {
        GameObject GlowingSphere = GameObject.Find(this.gameObject.name + "/System/Glowing Sphere");
        GameObject InnerEffect = GameObject.Find(this.gameObject.name + "/System/InnerEffect");
        GameObject Smoke = GameObject.Find(this.gameObject.name + "/System/Smoke");
        GameObject Electricity = GameObject.Find(this.gameObject.name + "/System/Electricity");
        GameObject Sphere = GameObject.Find(this.gameObject.name + "/System/Sphere");

        GlowingSphere.SetActive(false);
        InnerEffect.SetActive(false);
        Smoke.SetActive(false);
        Electricity.SetActive(false);
        Sphere.SetActive(false);
    }
}
