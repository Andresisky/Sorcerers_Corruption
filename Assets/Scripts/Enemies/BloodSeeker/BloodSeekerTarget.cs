using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloodSeekerTarget : MonoBehaviour, IDamageable
{
    private BloodSeeker m_Enemy;


    private void Awake()
    {
        m_Enemy = GetComponent<BloodSeeker>();
    }

    public void TakeDamage(float damageAmount)
    {
        m_Enemy.health -= damageAmount;
        if (m_Enemy.health <= 0)
        {
            m_Enemy.Death();
        }
    }

    public void Damage(float damageAmount)
    {
        TakeDamage(damageAmount);
    }
}
