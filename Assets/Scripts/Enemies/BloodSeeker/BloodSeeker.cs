using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class BloodSeeker : EnemyChaser, IDamageable
{
    #region Attributes

    private Animator anim;
    public GameObject styles;
    private SkinnedMeshRenderer[] mesh;


    //VFX
    [SerializeField] private SkinnedMeshRenderer skin;
    public VisualEffect vfxGraph;
    private Material[] dissolveMats;
    [SerializeField] private List<Material> dissolveMaterialStyle;
    private bool dissolve = false;

    [SerializeField] float dissolveRate = 0.02f;
    public float refreshRate = 0.05f;

    //Booleans

    #endregion

    #region Methods
    //Awake
    protected override void Awake()
    {
        base.Awake();
        anim = GetComponent<Animator>();
        
        GetRagdollBits();
        RagdollModeOff();
    }

    //Start
    protected override void Start()
    {
        base.Start();
        mesh = styles.GetComponentsInChildren<SkinnedMeshRenderer>();
        if (vfxGraph != null)
        {
            dissolve = false;
            vfxGraph.Stop();
            vfxGraph.gameObject.SetActive(false);
        }
        if (skin != null)
        {
            dissolveMats = skin.materials;
        }
        Style();
    }

    //Update
    protected override void Update()
    {
        base.Update();
        anim.SetBool("isRunning", false);
        /*if (PlayerInSightRange && !PlayerInAttackRange)
        {
            anim.SetBool("isRunning", true);
        }*/
        if (Agent.velocity.magnitude > 0)
        {
            anim.SetBool("isRunning", true);
        }
        else
        {
            anim.SetBool("isRunning", false);
        }
    }

    //CheckHitOnAnimation
    public void CheckHitOnAnimation()
    {
        if (PlayerInAttackRange && !Player.GetComponent<PlayerSystem>().isBlocking)
        {
            Player.GetComponent<PlayerSystem>().TakeDamage(EnemyDamage);
        }
        if (PlayerInAttackRange && Player.GetComponent<PlayerSystem>().isBlocking)
        {
            Player.GetComponent<PlayerSystem>().TakeDamage(EnemyDamage / 2);
        }
    }

    //Dissolve
    IEnumerator Dissolve()
    {
        float counter = 0;
        float counter2 = 0;
        if (vfxGraph != null)
        {
            vfxGraph.gameObject.SetActive(true);
            vfxGraph.Play();
        }
        if (dissolveMats.Length > 0)
        {
            while (dissolveMats[0].GetFloat("DissolveAmount_") < 1)
            {
                counter += dissolveRate;
                counter2 += dissolveRate;
                for (int i = 0; i < dissolveMats.Length; i++)
                {
                    dissolveMats[i].SetFloat("DissolveAmount_", counter);
                }
                for (int i = 0; i < dissolveMaterialStyle.Count; i++)
                {
                    dissolveMaterialStyle[i].SetFloat("DissolveAmount_", counter2);
                }
                yield return new WaitForSeconds(refreshRate);
                dissolve = true;
            }
        }
        Destroy(gameObject,0.2f);
    }

    #endregion


    #region StateMachine

    //Death
    public override void Death()
    {
        RagdollModeOn();
        Agent.speed = 0;
        Agent.enabled = false;
        for (int i = 0; i < ragdollColliders.Length; i++)
        {
            Physics.IgnoreCollision(Player.GetComponent<Collider>(), ragdollColliders[i]);
        }
        if (!dissolve)
        {
            StartCoroutine(Dissolve());
        }
        //Destroy(gameObject, 7f);
    }

    //Attack
    protected override void Attack()
    {
        base.Attack();
        if (!AlreadyAttacked)
        {
            //Attack Code here
            if (!IsDead)
            {
                anim.SetTrigger("canAttack");
                Agent.speed = 0;
            }

            AlreadyAttacked = true;
            Invoke(nameof(ResetAttack), TimeBetweenAttacks);
            //

        }
    }

    //Take Damage
    public void TakeDamage(float damageAmount)
    {
        Health -= damageAmount;
        if (health > 0)
        {
            anim.SetInteger("HitIndex", Random.Range(0, 3));
            anim.SetTrigger("takeDamage");
        }
        if (Health <= 0)
        {
            Death();
        }
    }



    //ResetAttack
    protected override void ResetAttack()
    {
        base.ResetAttack();
    }

    //FollowPlayer
    protected override void FollowPlayer()
    {
        Agent.speed = Speed;
        
        if (!IsDead || !IsStunned)
        {
            if (!AlreadyAttacked)
            {
                Agent.isStopped = false;
                Agent.SetDestination(Player.transform.position);
            }
        }
    }

    //Style
    private void Style()
    {
        int randomItemsNumber = Random.Range(0, mesh.Length + 2);
        for (int i = 0; i < randomItemsNumber; i++)
        {
            int randomSpawn = Random.Range(0, mesh.Length);
            mesh[randomSpawn].enabled = true;
            dissolveMaterialStyle.Add(mesh[randomSpawn].material);
        }
    }

    #endregion

    #region RagdollMode

    public Collider mainCollider;
    public GameObject rig;
    Collider[] ragdollColliders;
    Rigidbody[] rigidbodies;

    void RagdollModeOn()
    {
        anim.enabled = false;
        foreach (Collider col in ragdollColliders)
        {
            col.enabled = true;
        }

        foreach (Rigidbody rigid in rigidbodies)
        {
            rigid.isKinematic = false;
        }
        mainCollider.enabled = false;
        GetComponent<Rigidbody>().isKinematic = true;
    }
    void RagdollModeOff()
    {
        anim.enabled = true;
        foreach (Collider col in ragdollColliders)
        {
            col.enabled = false;
        }

        foreach (Rigidbody rigid in rigidbodies)
        {
            rigid.isKinematic = true;
        }
        mainCollider.enabled = true;
        GetComponent<Rigidbody>().isKinematic = false;
    }

    void GetRagdollBits()
    {
        ragdollColliders = rig.GetComponentsInChildren<Collider>();
        rigidbodies = rig.GetComponentsInChildren<Rigidbody>();
    }

    #endregion

    public void Damage(float damageAmount)
    {
        TakeDamage(damageAmount);
    }

}
