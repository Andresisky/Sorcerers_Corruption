using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTarget : MonoBehaviour, IDamageable
{
    private Enemy m_Enemy;


    private void Awake()
    {
        m_Enemy = GetComponent<Enemy>();
    }

    public void TakeDamage(float damageAmount)
    {
        m_Enemy.health -= damageAmount;
        if (m_Enemy.health <= 0)
        {
            m_Enemy.Death();
        }
    }

    public void Damage(float damageAmount)
    {
        if (m_Enemy != null)
        {
            TakeDamage(damageAmount);
        }
    }

}
