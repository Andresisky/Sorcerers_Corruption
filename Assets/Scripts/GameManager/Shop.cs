using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.UI;
using TMPro;

public class Shop : MonoBehaviour, IInteractable
{
    private GameObject player;
    private PlayerSystem playerSystem;
    private PlayerMovement playerMovement;
    private PlayerInputs playerInputs;
    private WeaponManager weaponManager;

    private GameObject vcam;
    private CinemachineVirtualCamera fpsCam;

    [SerializeField] GameObject shopUI;
    //Transform playerUI;

    [SerializeField] GameObject interactUI;


    [Header("Player Variables Texts")]
    [SerializeField] TMP_Text goldTxt;
    [SerializeField] TMP_Text currentHpTxt;
    [SerializeField] TMP_Text maxHpTxt;
    [SerializeField] TMP_Text currentMagityTxt;
    [SerializeField] TMP_Text maxMagityTxt;
    [SerializeField] TMP_Text livesTxt;
    [SerializeField] TMP_Text damageTxt;

    private float maxHP;
    private float maxMagity;
    private float damage;

    [Header("Skills Variables")]
    public SkillsSO[] skill;
    public GameObject skillHoldersPrefab;
    public GameObject skillsLayout;
    [HideInInspector] public List<GameObject> skillHolderUI;


    [Header("Items Variables")]
    public ItemsSO[] items;
    [SerializeField] private GameObject ItemPanelHolder;
    [SerializeField] private TMP_Text itemName;
    [SerializeField] private Image itemImage;
    [SerializeField] private TMP_Text itemDescription;
    [SerializeField] private TMP_Text itemDuration;
    [SerializeField] private TMP_Text itemQuantity;
    [SerializeField] private TMP_Text itemPrice;
    [SerializeField] private Button buyButton;

    //Sounds
    [Header("Sound Manager")]
    [SerializeField] private AudioSource audioSource;
    public AudioClip[] cheapSounds;
    public AudioClip[] expensiveSounds;
    [SerializeField] private AudioClip negationSFX;
    [SerializeField] private AudioClip newChanceSFX;



    private void Awake()
    {
        shopUI.SetActive(false);
        player = GameObject.Find("Player");
        //playerUI = player.transform.Find("HUD_Player");
        weaponManager = FindObjectOfType<WeaponManager>();
        playerSystem = player.GetComponent<PlayerSystem>();
        playerMovement = player.GetComponent<PlayerMovement>();
        playerInputs = player.GetComponent<PlayerInputs>();
        vcam = GameObject.Find("Player/CM vcam1");
        fpsCam = vcam.GetComponent<CinemachineVirtualCamera>();
        audioSource = GetComponent<AudioSource>();

        maxHP = playerSystem.maxHP;
        maxMagity = playerSystem.maxMagity;
        damage = playerSystem.currentDamage;

        for (int i = 0; i < skill.Length; i++)
        {
            skillHolderUI.Add(skillHoldersPrefab);

            skillHolderUI[i] = Instantiate(skillHoldersPrefab);
            skillHolderUI[i].transform.SetParent(skillsLayout.transform, false);

            skillHolderUI[i].GetComponent<SkillHolder>().icon.sprite = skill[i].skillIconPNG;
            skillHolderUI[i].GetComponent<SkillHolder>().magityCost.text = skill[i].magityCost.ToString();
            skillHolderUI[i].GetComponent<SkillHolder>().cooldownTime.text = skill[i].cooldownTime.ToString();
        }
    }

    private void Update()
    {
        goldTxt.text = playerSystem.money.ToString();
        currentHpTxt.text = playerSystem.hp.ToString();
        maxHpTxt.text = playerSystem.maxHP.ToString();
        currentMagityTxt.text = playerSystem.magity.ToString();
        maxMagityTxt.text = playerSystem.maxMagity.ToString();
        livesTxt.text = playerSystem.hearts.ToString();
        damageTxt.text = weaponManager.damage.ToString();
    }


    public void Interact()
    {
        shopUI.SetActive(true);
        playerSystem.DisableHUD();
        playerSystem.DisableMovement();

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    public void CloseShop()
    {
        ItemPanelHolder.SetActive(false);
        shopUI.SetActive(false);
        playerSystem.EnableMovement();
        playerSystem.EnableHUD();

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    public void SetItemPanel(ItemsSO item)
    {
        if (ItemPanelHolder.activeSelf == false)
        {
            ItemPanelHolder.SetActive(true);
        }
        itemName.text = item.itemName;
        itemImage.sprite = item.itemIcon;
        itemDescription.text = item.description;
        if (item.BuffDuration > 99)
        {
            itemDuration.text = "Unlimited while equipped";
        }
        else
        {
            if (item.BuffDuration < 0)
            {
                itemDuration.text = "Instant";
            }
            else
            {
                itemDuration.text = item.BuffDuration.ToString();
            }
        }
        
        itemQuantity.text = item.StackPerPurchase.ToString();
        itemPrice.text = item.Price.ToString();
        buyButton.onClick.RemoveAllListeners();
        buyButton.onClick.AddListener(delegate { BuyButton(item.Price, item); });
    }

    public void BuyButton(int price, ItemsSO item)
    {
        if (playerSystem.money >= price)
        {
            var inventory = player.GetComponent<InventoryHolder>();
            if (!inventory) return;
            if (inventory.InventoryManager.AddToInventory(item, item.StackPerPurchase))
            {
                Debug.Log(item.itemName + " Added to the Inventory!");
                playerSystem.money -= price;
                if (price > 500)
                {
                    audioSource.PlayOneShot(expensiveSounds[Random.Range(0, expensiveSounds.Length)]);
                }
                else
                {
                    audioSource.PlayOneShot(cheapSounds[Random.Range(0, cheapSounds.Length)]);
                }
            }
            else
            {
                audioSource.PlayOneShot(negationSFX);
            }
            //InventoryManager.Instance.Add(item);
            if (item.id == "item_extralife")
            {
                playerSystem.hearts++;
                inventory.InventoryManager.RemoveFromInventory(item, item.StackPerPurchase);
                audioSource.PlayOneShot(newChanceSFX);
            }
            
        }
        else
        {
            audioSource.PlayOneShot(negationSFX);
            Debug.Log("Not enough money!");
        }
    }

    public void UpgradeButton(int cost)
    {
        if (playerSystem.money >= cost)
        {
            UpgradeHP();
            playerSystem.money -= cost;
            cost += (cost / 5);
        }
        playerSystem.UpdateHUD();
    }

    #region Upgradables


    void UpgradeHP()
    {
        playerSystem.maxHP += 2;
        
    }

    #endregion


    public void ShowInteractUI()
    {
        interactUI.SetActive(true);
    }

    public void HideInteractUI()
    {
        interactUI.SetActive(false);
    }

}
