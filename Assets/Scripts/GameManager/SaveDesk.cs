using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SaveDesk : MonoBehaviour, IInteractable
{
    [SerializeField] PlayerSystem player;
    [SerializeField] InventoryHolder inventory;
    [SerializeField] WeaponManager weaponManager;
    [SerializeField] private GameObject saveUI;
    [SerializeField] Transform playerUI;


    public void Awake()
    {
        player = FindObjectOfType<PlayerSystem>();
        weaponManager = FindObjectOfType<WeaponManager>();
        inventory = FindObjectOfType<InventoryHolder>();
        playerUI = player.transform.Find("HUD_Player");
    }

    public void SaveGame()
    {
        SaveManager.SavePlayerData(player);
        //SaveManager.SaveInventoryData(inventory);
        Debug.Log("Game Saved");
    }

    public void LoadGame()
    {
        PlayerData playerData = SaveManager.LoadPlayerData();
        player.hearts = playerData.hearts;
        player.maxHP = playerData.maxHealth;
        player.hp = playerData.health;
        player.maxMagity = playerData.maxMagity;
        player.magity = playerData.magity;
        player.damage = playerData.damage;
        player.money = playerData.money;
        player.UpdateHUD();
        /*InventoryData inventoryData = SaveManager.LoadInventoryData();
        inventory.InventorySize = inventoryData.inventorySize;
        for (int i = 0; i < inventory.InventorySize; i++)
        {
            inventory.InventoryManager.InventorySlots[i].AssignItem(inventoryData.slot[i]);
        }*/
        Debug.Log("Files Loaded");
    }

    public void Interact()
    {
        saveUI.SetActive(true);
        playerUI.gameObject.SetActive(false);
        player.DisableMovement();

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    public void CloseUI()
    {
        saveUI.SetActive(false);
        player.EnableMovement();
        playerUI.gameObject.SetActive(true);

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    public void ShowInteractUI()
    {
        //interactUI.SetActive(true);
    }

    public void HideInteractUI()
    {
        //interactUI.SetActive(false);
    }
}
