using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "ItemName", menuName = "ScriptableObject/Items")]
public class ItemsSO : ScriptableObject
{
    public string id;
    public string itemName;
    [SerializeField] private int price;
    [SerializeField] private int maxStackSize;
    [SerializeField] private int stackPerPurchase;
    [SerializeField] private int buffDuration;
    [SerializeField] private float buffTimer;
    public Sprite itemIcon;
    public Sprite itemIconPNG;
    [SerializeField] private AudioClip sound;
    public ItemType itemType;
    [TextArea] public string description;
    

    public enum ItemType
    {
        Potion,
        Equippable,
        Instant
    }

    #region Properties

    public int Price
    {
        get { return price; }
    }

    public int MaxStackSize
    {
        get { return maxStackSize; }
    }

    public AudioClip Sound
    {
        get { return sound; }
    }

    public int BuffDuration
    {
        get { return buffDuration; }
    }

    public float BuffTimer
    {
        get { return buffTimer; }
        set { buffTimer = value; }
    }

    public int StackPerPurchase
    {
        get { return stackPerPurchase; }
    }

    #endregion



}
