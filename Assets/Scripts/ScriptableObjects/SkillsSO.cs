using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "SkillName", menuName = "ScriptableObject/Skills")]
public class SkillsSO : ScriptableObject
{
    public int number;
    [SerializeField] private string skillName;
    public int magityCost;
    public int cooldownTime;
    private float cooldownTimer;
    [SerializeField] private int buffDuration;
    private float buffDurationTimer;
    public Sprite skillIcon;
    public Sprite skillIconPNG;
    [TextArea]
    [SerializeField] string description;
    [SerializeField] private bool isActive;
    [SerializeField] private bool buffActive;


    public bool IsActive
    {
        get { return isActive; }
        set { isActive = value; }
    }
    public bool BuffActive
    {
        get { return buffActive; }
        set { buffActive = value; }
    }    
    public int CooldownTime
    {
        get { return cooldownTime; }
        set { cooldownTime = value; }
    }

    public float CooldownTimer
    {
        get { return cooldownTimer; }
        set { cooldownTimer = value; }
    }

    public float BuffDurationTimer
    {
        get { return buffDurationTimer; }
        set { buffDurationTimer = value; }
    }

    public int BuffDuration
    {
        get { return buffDuration; }
        set { buffDuration = value; }
    }

    /*public IEnumerator BuffDuration()
    {
        yield return new WaitForSeconds(buffDuration);
        buffActive = false;
    }*/
}

