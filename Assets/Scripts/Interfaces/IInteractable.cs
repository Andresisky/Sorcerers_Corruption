public interface IInteractable
{
    void Interact();
    void ShowInteractUI();
    void HideInteractUI();
}
