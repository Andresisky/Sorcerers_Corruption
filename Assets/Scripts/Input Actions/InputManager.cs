using System;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputManager : MonoBehaviour
{

    private static InputManager _instance;

    public static InputManager Instance
    {
        get
        {
            return _instance;
        }
    }

    private PlayerControls playerControls;

    //Actions
    private InputAction movement;



    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        playerControls = new PlayerControls();
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private void OnEnable()
    {
        playerControls.Enable();

        movement = playerControls.OnCombat.Movement;
        movement.Enable();

        playerControls.OnCombat.Jump.performed += Jump;
    }

    private void Jump(InputAction.CallbackContext obj)
    {
        Debug.Log("jump");
    }

    private void OnDisable()
    {
        movement.Disable();
        playerControls.Disable();
    }

    public Vector2 GetPlayerMovement()
    {
        return playerControls.OnCombat.Movement.ReadValue<Vector2>();
    }

    public Vector2 GetMouseDelta()
    {
        return playerControls.OnCombat.Look.ReadValue<Vector2>();
    }

    public bool Sprint()
    {
        return playerControls.OnCombat.Sprint.IsPressed();
    }

    public bool PlayerJumpedThisFrame()
    {
        return playerControls.OnCombat.Jump.triggered;
    }

    public bool PlayerAttackedThisFrame()
    {
        return playerControls.OnCombat.Attack.triggered;
    }

    public bool PlayerBlockedThisFrame()
    {
        return playerControls.OnCombat.Block.IsPressed();
    }

    public bool DeadlyEdgeActived()
    {
        return playerControls.OnCombat.DeadlyEdge.triggered;
    }
    public bool FoxosBlessingActived()
    {
        return playerControls.OnCombat.FoxosBlessing.triggered;
    }
    public bool UltrasoundActived()
    {
        return playerControls.OnCombat.Ultrasound.triggered;
    }
    public bool SoulArrowActived()
    {
        return playerControls.OnCombat.SoulArrow.triggered;
    }
    public bool Item1Actived()
    {
        return playerControls.OnCombat.Item1.triggered;
    }
    public bool Item2Actived()
    {
        return playerControls.OnCombat.Item2.triggered;
    }

    private void FixedUpdate()
    {
        movement.ReadValue<Vector2>();
    }

}
