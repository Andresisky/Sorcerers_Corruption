using System;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerInputs : MonoBehaviour
{
    private PlayerMovement playerMovement;
    private WeaponManager weaponManager;

    //[SerializeField] Skills skills;

    private PlayerControls playerControls;

    Vector2 currentMovement;

    private static PlayerInputs _instance;

    public static PlayerInputs Instance
    {
        get
        {
            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        playerMovement = FindObjectOfType<PlayerMovement>();
        weaponManager = FindObjectOfType<WeaponManager>();

        playerControls = new PlayerControls();

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        playerControls.OnCombat.Movement.performed += ctx => currentMovement = ctx.ReadValue<Vector2>();
        //playerControls.OnCombat.Jump.performed += _ => playerMovement.OnJump();

        playerControls.OnCombat.Sprint.performed += _ => playerMovement.OnSprint();
        playerControls.OnCombat.Sprint.canceled += _ => playerMovement.NotSprint();

        playerControls.OnCombat.Block.performed += _ => weaponManager.IsShielded();
        playerControls.OnCombat.Block.canceled += _ => weaponManager.NotShielded();

    }

    private void OnEnable()
    {
        playerControls.OnCombat.Enable();
    }

    private void OnDisable()
    {
        playerControls.OnCombat.Disable();
    }

    private void Update()
    {
        playerMovement.ReceiveInput(currentMovement);
    }

    public bool Jump()
    {
        return playerControls.OnCombat.Jump.triggered;
    }

    public bool DeadlyEdgeActivated()
    {
        return playerControls.OnCombat.DeadlyEdge.triggered;
    }

    public bool Attack()
    {
        return playerControls.OnCombat.Attack.triggered;
    }

    public bool FoxosBlessingActivated()
    {
        return playerControls.OnCombat.FoxosBlessing.triggered;
    }

    public bool UltrasoundActivated()
    {
        return playerControls.OnCombat.Ultrasound.triggered;
    }

    public bool SoulArrowActivated()
    {
        return playerControls.OnCombat.SoulArrow.triggered;
    }

    public bool Item1()
    {
        return playerControls.OnCombat.Item1.triggered;
    }

    public bool Item2()
    {
        return playerControls.OnCombat.Item2.triggered;
    }

    public bool Interact()
    {
        return playerControls.OnCombat.Interact.triggered;
    }

    public bool Inventory()
    {
        return playerControls.OnCombat.Inventory.triggered;
    }

    public bool PauseUnpause()
    {
        return playerControls.OnCombat.Pause.triggered;
    }

    public bool Screenshot()
    {
        return playerControls.OnCombat.TakeScreenshot.triggered;
    }
}
