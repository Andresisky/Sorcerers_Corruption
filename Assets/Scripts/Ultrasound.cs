using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ultrasound : MonoBehaviour
{
    [SerializeField] Vector3 minScale;
    private Vector3 minScaleAux;
    public Vector3 maxScale;
    public float speed;
    public float duration;
    private AudioSource audioSource;
    [SerializeField] private AudioClip impact;

    private void Awake()
    {
        minScale = transform.localScale;
        minScaleAux = minScale;
        audioSource = GetComponent<AudioSource>();
    }

    void OnEnable()
    {
        minScale = minScaleAux;
        StartCoroutine(OnEnableCoroutine());
    }


    public IEnumerator GrowSphere(Vector3 a, Vector3 b, float time)
    {
        float i = 0.0f;
        float rate = (1.0f / time) * speed;
        while(i < 1.0f)
        {
            i += Time.deltaTime * rate;
            transform.localScale = Vector3.Lerp(a, b, i);
            yield return null;
        }
        gameObject.SetActive(false);
    }


    private IEnumerator OnEnableCoroutine()
    {
        yield return GrowSphere(minScale, maxScale, duration);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            audioSource.PlayOneShot(impact);
        }    
    }

}
