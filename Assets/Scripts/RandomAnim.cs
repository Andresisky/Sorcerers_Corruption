using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomAnim : MonoBehaviour
{
    private Animator anim;
    public int number;
    IEnumerator Start()
    {
        anim = GetComponent<Animator>();

        while (true)
        {
            number = Random.Range(1, 4);
            yield return new WaitForSeconds(3);
            anim.SetTrigger("Hit");
            anim.SetInteger("HitIndex", number);
        }
    }
}
