using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SettingsMenu))]
public class SettingsMenuEditor : Editor
{
    public enum DisplayCategory
    {
        Video, Audio, Controls, Other
    }

    public DisplayCategory categoryToDisplay;

    public override void OnInspectorGUI()
    {
        categoryToDisplay = (DisplayCategory)EditorGUILayout.EnumPopup("Display", categoryToDisplay);
        EditorGUILayout.Space();


        switch (categoryToDisplay)
        {
            case DisplayCategory.Video:
                DisplayVideoInfo();
                break;

            case DisplayCategory.Audio:
                DisplayAudioInfo();
                break;

            case DisplayCategory.Controls:
                DisplayControlsInfo();
                break;

            case DisplayCategory.Other:
                DisplayOtherInfo();
                break;
        }


        serializedObject.ApplyModifiedProperties();
    }

    void DisplayVideoInfo()
    {
        EditorGUILayout.PropertyField(serializedObject.FindProperty("videoPanel"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("resolutionDropdown"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("graphicsDropdown"));
    }

    void DisplayAudioInfo()
    {
        EditorGUILayout.PropertyField(serializedObject.FindProperty("audioPanel"));

    }

    void DisplayControlsInfo()
    {
        EditorGUILayout.PropertyField(serializedObject.FindProperty("controlsPanel"));
    }

    void DisplayOtherInfo()
    {
        EditorGUILayout.PropertyField(serializedObject.FindProperty("otherPanel"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("applyChangesButton"));
    }
}
